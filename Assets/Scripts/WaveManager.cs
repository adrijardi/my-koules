﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveManager : MonoBehaviour {

	public float nextWaveTimer = 10;
	public int totalWaves = 10;

	[SerializeField]
	private int enemiesInWave = 5;
	[SerializeField]
	private Transform spawnArea;
	[SerializeField]
	private ObjectProbabilityPair[] enemies;
	private LevelManager levelManager;

	private RandomObjectProvider randomObjectProvider;
	private float timeForNextWave;
	private int wavesLeft;

	void Start()
	{
		timeForNextWave = nextWaveTimer;
		wavesLeft = totalWaves;
		levelManager = GameObject.FindGameObjectWithTag ("GameController").GetComponent<LevelManager> ();
		randomObjectProvider = new RandomObjectProvider (enemies);
	}

	void Update () 
	{
		if (wavesLeft > 0 && levelManager.isPlaying()) 
		{
			timeForNextWave -= Time.deltaTime;
			if (timeForNextWave <= 0) 
			{
				SpawnWave ();
			}
		}
	}

	public void SpawnWave ()
	{
		timeForNextWave = nextWaveTimer;
		for (int i = 0; i < enemiesInWave; i++) {
			var randomPos = new Vector3 (Random.Range (-1f, 1f), 0, Random.Range (-1f, 1f)) * .5f;
			var spawnPos = spawnArea.TransformPoint (randomPos);
			var enemy = randomObjectProvider.getRandomObject();
			Instantiate (enemy._2, spawnPos, Quaternion.identity);
		}
	}

	//private getRandomEnemy()
}

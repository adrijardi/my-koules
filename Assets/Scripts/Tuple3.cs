﻿using System;

public class Tuple3<A, B, C> {

	public A _1;
	public B _2;
	public C _3;

	public Tuple3(A a, B b, C c)
	{
		_1 = a;
		_2 = b;
		_3 = c;
	}
}

﻿using UnityEngine;
using System;

[Serializable]
public class EnemyProbabilityTuple : Tuple2<GameObject, float> {
	public EnemyProbabilityTuple (GameObject enemy, float probability) : base (enemy, probability) {}
}

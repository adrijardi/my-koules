﻿using System;
using UnityEngine;

[System.Serializable]
public class ObjectProbabilityPair : Tuple3<float, GameObject, ParticleSystem>
{
	public ObjectProbabilityPair (float probability, GameObject go, ParticleSystem particle) : base(probability, go, particle) {}
}


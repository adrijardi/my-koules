﻿using UnityEngine;

public class DeathBox : MonoBehaviour {

	void OnTriggerEnter(Collider c) {
		var d = c.GetComponent<Destructible> ();
		if (d != null) {
			d.Destroy ();
		}
	}
}

﻿using System;

public class Tuple2<A, B> {

	public A _1;
	public B _2;

	public Tuple2(A a, B b)
	{
		_1 = a;
		_2 = b;
	}
}

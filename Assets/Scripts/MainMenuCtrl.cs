﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LevelManager))]
public class MainMenuCtrl : MonoBehaviour {

    [SerializeField]
    private Animator animator;
    private LevelManager levelManager;

    private void Start() {
        levelManager = GetComponent<LevelManager>();
    }

    void Update() {
        if (levelManager.isPlaying()) {
            CheckForMainMenu();
        }
    }

    public void MainMenu() {
        animator.SetTrigger("mainMenu");
        animator.SetInteger("currentMenu", 0);
    }

    public void SetMenu(int menu) {
        animator.SetInteger("currentMenu", menu);
    }

    private void CheckForMainMenu() {
        if (Input.GetKeyUp(KeyCode.Escape)) {
            levelManager.SetPlaying(false);
            MainMenu();
        }
    }

    public void QuitGame() {
        Application.Quit(); 
    }
}

﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class CollisionScript : MonoBehaviour {

	[SerializeField]
	private float forceAmplifier = 50;
	private Rigidbody rb;

	void Start() {
		rb = GetComponent<Rigidbody> ();
	}

	void OnCollisionEnter(Collision c) {
		var otherRb = c.rigidbody;
        if(otherRb != null) { 
		    var otherForce = 1 + (rb.mass / otherRb.mass);
		    otherRb.AddExplosionForce (otherForce * forceAmplifier, transform.position, 100);
		    var myForce = 1 + (otherRb.mass / rb.mass);
		    rb.AddExplosionForce (myForce * forceAmplifier, c.transform.position, 100);
        }
    }
}

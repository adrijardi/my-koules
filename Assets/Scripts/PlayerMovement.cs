﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerMovement : MonoBehaviour {

	[SerializeField]
	public float movementForce = 100;
	private Rigidbody rb;
	[SerializeField]
	private float rotationSpeed = 10f;
    [SerializeField]
    private ParticleSystem trail;
    [SerializeField]
    private string inputPlayerName;
	private LevelManager levelManager;

	void Start () 
	{
		rb = GetComponent<Rigidbody> ();
		levelManager = GameObject.FindGameObjectWithTag ("GameController").GetComponent<LevelManager> ();
	}

	void FixedUpdate () 
	{
		if (levelManager.isPlaying ()) 
		{
			var h = Input.GetAxis (inputPlayerName+"-Horizontal");
			var v = Input.GetAxis (inputPlayerName+"-Vertical");

			if (h != 0 || v != 0) 
			{
				var target = new Vector3 (h, 0, v);
				var desiredRotation = Quaternion.LookRotation (target);
				transform.rotation = Quaternion.RotateTowards (transform.rotation, desiredRotation, rotationSpeed * Time.deltaTime);

				rb.AddForce (transform.forward * movementForce * rb.mass * Time.deltaTime);
                trail.Play();
            }
            else {
                trail.Stop();
            }

            AddMassiveObjectForces ();
		}
	}

	private void AddMassiveObjectForces()
	{
		var massive = GameObject.FindGameObjectsWithTag ("Massive");
		foreach(var m in massive)
		{
			var vector = m.transform.position - transform.position;
			var forceDirection = vector.normalized;
			var distance = vector.magnitude;
			var mass = 200;
			var force = forceDirection * mass / distance;
			rb.AddForce (force * Time.deltaTime);
		}
	}
}

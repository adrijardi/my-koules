﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class EnemyMovement : MonoBehaviour {

	[SerializeField]
	private float force = 100;
	private Rigidbody rb;
	private LevelManager levelManager;

	void Start() {
		rb = GetComponent<Rigidbody> ();
		levelManager = GameObject.FindGameObjectWithTag ("GameController").GetComponent<LevelManager> ();
	}

	void FixedUpdate () {
		if (levelManager.isPlaying ()) {
			var target = FindClosestPlayer ();
			if (target != null) {
				var direction = (target.transform.position - transform.position).normalized;
				rb.AddForce (direction * force);
			}
		}
	}

	GameObject FindClosestPlayer() { 
		var players = GameObject.FindGameObjectsWithTag ("Player");

		GameObject closest = null;
		var closestDistance = float.MaxValue;

		foreach (var player in players) {
			var distance = (transform.position - player.transform.position).magnitude;
			if (distance < closestDistance) {
				closest = player;
				closestDistance = distance;
			}
		}
		return closest;
	}
}

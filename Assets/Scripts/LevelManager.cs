﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[RequireComponent(typeof(WaveManager))]
public class LevelManager : MonoBehaviour {

	[SerializeField]
	private Animator animator;
	[SerializeField]
	private GameObject[] players;
	[SerializeField]
	private GameObject enemy;
	private WaveManager waveManager;

	private bool playing = false;
	private bool gameOver = false;
	[SerializeField]
	private float gameOverTimeout = 3;
	private float gameOverTimer;

	public int CurrentLevel { get; private set; }
	[SerializeField]
	private Text newLevelText;
    [SerializeField]
    private Button continueButton;

    private int currentNumPlayers;

	void Start()
	{
		waveManager = GetComponent<WaveManager> ();
		CurrentLevel = 1;
        continueButton.interactable = false;
    }

	public bool isPlaying()
	{
		return playing;
	}

    public void NewGame(int numPlayers) {
        CurrentLevel = 1;
        continueButton.interactable = true;
        RestartGame(numPlayers);
    }

    public void RestartGame() {
        RestartGame(currentNumPlayers);
    }

    private void RestartGame(int numPlayers) 
	{
        currentNumPlayers = numPlayers;

        foreach (var p in GameObject.FindGameObjectsWithTag ("Player")) {
			Destroy (p);
		}
		foreach (var e in GameObject.FindGameObjectsWithTag ("Enemy")) {
			Destroy (e);
		}
        foreach (var e in GameObject.FindGameObjectsWithTag("Massive")) {
            Destroy(e);
        }
        foreach (var e in GameObject.FindGameObjectsWithTag("OtherEnemy")) {
            Destroy(e);
        }

        var spawnPoints = FindPlayerSpawnPoints ();
        for(int i = 0; i < numPlayers; i++) {
            Instantiate(players[i], spawnPoints[i].transform.position, spawnPoints[i].transform.rotation);
        }
		
		waveManager.SpawnWave ();
		SetPlaying (true);
		SetGameOver (false);
	}

	private GameObject[] FindPlayerSpawnPoints()
	{
		return GameObject.FindGameObjectsWithTag("Respawn");
	}

    public void ContinueGame() {
        SetPlaying(true);
    }

	void Update() {
		if (playing) {
			CheckForGameOver ();
			CheckForWin ();
		} else if (gameOver) {
			CheckForRestart ();
		}
	}

	private void CheckForGameOver()
	{
		var noPlayers = GameObject.FindGameObjectWithTag ("Player") == null;
		if (noPlayers) {
			SetGameOver (true);
			SetPlaying (false);
		}
	}

	private void CheckForWin()
	{
		var noEnemies = GameObject.FindGameObjectWithTag("Enemy") == null;
		if (noEnemies) {
			++CurrentLevel;
			newLevelText.text = "Level " + CurrentLevel;

			animator.SetTrigger ("victory");
			SetPlaying (false);
			Invoke ("RestartGame", 2);
		}
	}

	private void CheckForRestart()
	{
		gameOverTimer += Time.deltaTime;
		if (Input.anyKeyDown && gameOverTimer > gameOverTimeout) 
		{
            RestartGame(currentNumPlayers);
		}
	}

	private void SetGameOver(bool setGameOver)
	{
		gameOver = setGameOver;
		if (setGameOver) {
            continueButton.interactable = false;
            animator.SetTrigger ("gameover");
			gameOverTimer = 0f;
		}
	}

	public void SetPlaying(bool setPlaying)
	{
		playing = setPlaying;
		if(setPlaying)
			animator.SetTrigger("restartGame");
	}
		
}

﻿using UnityEngine;

public class MassPowerUp : PowerUp {

	override protected void action(GameObject player) {
		var rb = player.GetComponent<Rigidbody> ();
		rb.mass += powerUpAmount;
	}
}

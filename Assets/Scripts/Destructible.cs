﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destructible : MonoBehaviour {

	[SerializeField]
	private GameObject destructionAnimation;

	protected bool invulnerable;

	void Awake()
	{
		invulnerable = true;
		StartCoroutine (RemoveInvulnerability());
	}


	private IEnumerator RemoveInvulnerability()
	{
		yield return new WaitForSeconds (.1f);
		invulnerable = false;
	}

	public void Destroy() 
	{
		if(!invulnerable) 
		{
			DestroyAction ();
		}
	}

	virtual protected void DestroyAction() 
	{
		DestructionAnimation ();
		Destroy (gameObject);
	}

	protected void DestructionAnimation() {
		var anim = Instantiate (destructionAnimation, transform.position, transform.rotation) as GameObject;
		Destroy (anim, 3f);
	}
}

﻿using UnityEngine;

public abstract class PowerUp : MonoBehaviour {

	[SerializeField]
	protected float powerUpAmount = 100;
	[SerializeField]
	protected GameObject replaceWith;

	protected abstract void action(GameObject player);

	void OnCollisionEnter(Collision c){
		var other = c.gameObject;
		if (other.CompareTag ("Player")) {
			action (other);

			Destroy (gameObject);
			Instantiate (replaceWith, transform.position, transform.rotation);
		}
	}
}

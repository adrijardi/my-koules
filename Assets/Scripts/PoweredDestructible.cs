﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoweredDestructible : Destructible {

	[SerializeField]
	private ObjectProbabilityPair[] spawnProbabilities;
	private RandomObjectProvider objectProvier;

	void Start()
	{
		objectProvier = new RandomObjectProvider(spawnProbabilities);
	}

	override protected void DestroyAction() 
	{
		if (!Spawn ())
		{
			DestructionAnimation ();
		}
		Destroy (gameObject);
	}

	private bool Spawn()
	{
		var enemy = objectProvier.getRandomObject ();
		if (enemy != null) {
			Instantiate (enemy._2, transform.position, transform.rotation);
			var anim = Instantiate (enemy._3, transform.position, transform.rotation) as ParticleSystem;
			Destroy (anim.gameObject, 3f);
			return true;
		}
		return false;
	}

}

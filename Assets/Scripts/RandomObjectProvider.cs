﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomObjectProvider
{
	private ObjectProbabilityPair[] objects;

	public RandomObjectProvider (ObjectProbabilityPair[] objects)
	{
		this.objects = objects;
	}

	public ObjectProbabilityPair getRandomObject()
	{
		var random = Random.Range (0f, 1f);
		foreach(var pair in objects) {
			random -= pair._1;
			if (random <= 0) {
				return pair;
			}
		}
		return null;
	}
}



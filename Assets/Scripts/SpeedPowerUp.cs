﻿using UnityEngine;

public class SpeedPowerUp : PowerUp {

	override protected void action(GameObject player) {
		var mov = player.GetComponent<PlayerMovement> ();
		mov.movementForce += powerUpAmount;
	}
}
